package com.imooc.ecomerce.filter.factory;

import com.imooc.ecomerce.filter.HeaderTokenGatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

/**
 * yml中配置名称取xxGatewayFilterFactory之前的xx
 *
 * @author 江黎
 * @link {com.imooc.ecomerce.filter.HeaderTokenGatewayFilter}
 * @date 2022/1/3 10:50
 **/
@Component
public class HeaderTokenGatewayFilterFactory extends AbstractGatewayFilterFactory<Object> {

    @Override
    public GatewayFilter apply(Object config) {
        return new HeaderTokenGatewayFilter();
    }
}
