package com.imooc.ecomerce.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 局部过滤器示例
 * HTTP请求头部携带Token的验证过滤器
 * 1.需要实现GatewayFilter, Ordered相关方法
 * 2.加入到过滤器工厂，并且讲工厂注册到spring容器中
 * 3.在配置文件使用，不配置使用则不启用此过滤器（路由规则）
 *
 * @author 江黎
 * @date 2022/1/3 10:50
 **/
public class HeaderTokenGatewayFilter implements GatewayFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 从 HTTP Header 中寻找 key 为 token, value 为 imooc 的键值对
        String token = exchange.getRequest().getHeaders().getFirst("token");
        if ("imooc".equals(token)) {
            return chain.filter(exchange);
        }
        // 标记此次请求没有权限, 并结束这次请求
        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        return exchange.getResponse().setComplete();
    }

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE + 2;
    }
}
