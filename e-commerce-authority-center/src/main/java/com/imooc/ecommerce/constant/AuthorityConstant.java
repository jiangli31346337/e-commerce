package com.imooc.ecommerce.constant;

import sun.misc.BASE64Decoder;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * <h1>授权需要使用的一些常量信息</h1>
 * */
public final class AuthorityConstant {

    /** RSA 私钥, 除了授权中心以外, 不暴露给任何客户端。公钥用来解密，放在common模块 */
    public static final String PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCcPD6YLiLeXDTLaEqt/4bal3AT3NcPWFQHtzzHkvaA4trT1/65jdmiugKsTV6nMn15cQJNhqV9zzVeGTtqESFRiTcdBOWYbruOg6+Nn9Kj33vPqRv5ft9Vk0pkJxGOKd9VWb05wqqm50bswg4LY+fuwOGAhLiCDoEocMXl6z2fKGMyfllbIOg63oJq8fIKuiYOc/e3iJ63ARzzyDKRYdPtZu2Xwt8cwEhuEQIGnDsgPhBWmFtQ3UJDXY8Wi7q+jiFU64keJogpWN8Unx/F/TMhYStf+mDdb1b9nUKExwCZjbeJcZsdxGQMjFMBftLx7RudEcflmvcMvHAyJ1XVAxXzAgMBAAECggEBAIIRwcbuOgdHWoixdeLwJjQb+an/pGfZEHQwAADjACgDNU1CuIWAa90G4SzTaObnqQCOre0fGO3psTN67DT8t/FqvCCDsCW8opceWOweT74wU6Jp1qlbD9HtwvQodnwVriIfiOLqhypWx3xafljLPw9NMy/MztMwAiVJ5efpda7nHWM37d+OecuTVvsG1yhaNWCr5hk+He8T17Ff+CN81h61HxLoES0+CwLO2wzYWZ3wNdWFviwsIwsyDXQVY4LsIlY0ErxBLW98LUrlPF6O6E/daW/TXgaZqumoRu9XpRA5fIvFdaK1as2ZZ+uyQFFDOZEhSg9PV0YFGV0ezjaHZakCgYEA75rtQ9TR8s7TfccR4NM3ZKFBEPgbF9gsetWg/wn3GDO20CxuyBoQ63hGvbLt8FpR9nDqC7jwjzX+xUC26B0df5kEbCzPY2tOS85RKSJognkvQHxsVMf2KOA0fbxE9nKfPUG4Fj6Xp9F7ioQEdxDpWpyjcUOloeTPa2kIqYu+818CgYEApuz1puKVk4X3Nx1q5do91gszVFQjv8KNkI8vVbVAzrYXvk2P0ZATyOQLrIrOIm4UXAvULT+Z3Hi7aPpOwqEkLxHE4p6jwrWY+BKLbpmn7FS09+AOOU40qkqucl2L569u50KMpJMmbBFhgasukHgZXbU/RHUuRGwwHfw9Aoc4me0CgYEA4g1iZ6pzYQZsVa/oWBotmozgHChXN0nFvSWrPLKkIns3sHMbGUIuUFkrNHGJNkegKEcjKGgU3Kfucx648Yf2eplcqMol5wAn4DEINSy3ONEoGUjJ07rLrIjKPQ3Qakp9nYdRK/tljPh/+5DNvZ+A6PH66TMPIwwB/K4ddAfZ4TUCgYBNWo3T0jiCbphLQCSjc2koYXRd5cfTOzMjBFdpHl9z60pBc+vrrxqcA6GtSX2Q0vY4WXrvpQOF+aTPVv9cjhQYg+F7Kwy0VDcY7m8Sx73bdL2QCJJO2ADr0UPZ16PXr6JzJJHMSiyJ89v9nPgtFpqapD03Wh4gswnxO0yCeWsvWQKBgCWgz3a7d0kKfMLDjqAxUOGdueTdTUoyCECopQCKEDkGETRfMRwwNzGpubiYVzSAuAxbWs5WVONy3BJm46lMaBLutCgOlMbTmcpfy4K9exZ5RDrb2/0EVA6L49lfIR9JYfCc457atrj2EU2wbT7Jt8jlBX2r1rFknOoD++rH7Sua";

    /** 默认的 Token 超时时间, 一天 */
    public static final Integer DEFAULT_EXPIRE_DAY = 1;

    /**
     * <h2>根据本地存储的私钥获取到 PrivateKey 对象</h2>
     */
    public static PrivateKey getPrivateKey() throws Exception {

        PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
                new BASE64Decoder().decodeBuffer(AuthorityConstant.PRIVATE_KEY));
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(priPKCS8);
    }
}
