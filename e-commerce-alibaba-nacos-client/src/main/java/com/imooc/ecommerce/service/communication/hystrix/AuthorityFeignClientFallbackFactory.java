package com.imooc.ecommerce.service.communication.hystrix;

import com.imooc.ecommerce.service.communication.AuthorityFeignClient;
import com.imooc.ecommerce.vo.JwtToken;
import com.imooc.ecommerce.vo.UsernameAndPassword;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * FallbackFactory能捕获feign调用的异常
 * Fallback不能捕获异常
 *
 * @author 江黎
 * @date 2022/1/8 21:51
 **/
@Slf4j
@Component
@Transactional(rollbackFor = Exception.class)
public class AuthorityFeignClientFallbackFactory implements FallbackFactory<AuthorityFeignClient> {


    @Override
    public AuthorityFeignClient create(Throwable throwable) {
        log.warn(" authority feign client get token by feign request error [{}]", throwable.getMessage());
        return new AuthorityFeignClient() {
            @Override
            public JwtToken getTokenByFeign(UsernameAndPassword usernameAndPassword) {
                return new JwtToken();
            }
        };
    }
}
