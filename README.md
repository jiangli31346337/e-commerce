**SpringCloudAlibaba微服务架构实战，从架构设计到** 7-1

**1.实现统一响应，对统一返回值进行包装 CommonResponseDataAdvice**

**2.搭建springboot admin应用监控**

**3.生成RSA私钥公钥 RSATest，通过私钥生成JwtToken->IJWTService，通过公钥解析生成LoginUserInfo->TokenParseUtil**

**4.授权和鉴权微服务，即生成token和解析token，生成过程放在授权中心，解析过程放在公共模块**

**5.gateway网关动态路由，GatewayConfig DynamicRouteServiceImpl DynamicRouteServiceImplByNacos**

**6.全局登录鉴权过滤器 GlobalCacheRequestBodyFilter GlobalLoginOrRegisterFilter**

**7.用户登录拦截器 LoginUserInfoInterceptor**

**8.枚举类GoodsStatus及转换器GoodsStatusConverter**

**9.自定义异步任务线程池, 异步任务异常捕获处理器 AsyncPoolConfig**

**10.集合求差集和redis的hash操作 GoodsServiceImpl**