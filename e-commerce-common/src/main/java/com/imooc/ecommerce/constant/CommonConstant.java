package com.imooc.ecommerce.constant;

/**
 * <h1>通用模块常量定义</h1>
 * */
public final class CommonConstant {

    /** RSA 公钥 */
    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnDw+mC4i3lw0y2hKrf+G2pdwE9zXD1hUB7c8x5L2gOLa09f+uY3ZoroCrE1epzJ9eXECTYalfc81Xhk7ahEhUYk3HQTlmG67joOvjZ/So997z6kb+X7fVZNKZCcRjinfVVm9OcKqpudG7MIOC2Pn7sDhgIS4gg6BKHDF5es9nyhjMn5ZWyDoOt6CavHyCromDnP3t4ietwEc88gykWHT7Wbtl8LfHMBIbhECBpw7ID4QVphbUN1CQ12PFou6vo4hVOuJHiaIKVjfFJ8fxf0zIWErX/pg3W9W/Z1ChMcAmY23iXGbHcRkDIxTAX7S8e0bnRHH5Zr3DLxwMidV1QMV8wIDAQAB";

    /** JWT 中存储用户信息的 key */
    public static final String JWT_USER_INFO_KEY = "e-commerce-user";

    /** 授权中心的 service-id */
    public static final String AUTHORITY_CENTER_SERVICE_ID = "e-commerce-authority-center";
}
