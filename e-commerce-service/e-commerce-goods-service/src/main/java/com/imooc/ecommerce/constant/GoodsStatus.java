package com.imooc.ecommerce.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * 商品状态枚举类
 *
 * @author 江黎
 * @date 2022/1/7 20:51
 **/
@Getter
@AllArgsConstructor
public enum GoodsStatus {

    /**
     * 上线
     */
    ONLINE(101, "上线"),
    /**
     * 下线
     */
    OFFLINE(102, "下线"),
    /**
     * 缺货
     */
    STOCK_OUT(103, "缺货"),
    ;

    /**
     * 状态码
     */
    private final Integer status;

    /**
     * 状态描述
     */
    private final String description;

    /**
     * <h2>根据 code 获取到 GoodsStatus</h2>
     */
    public static GoodsStatus of(Integer status) {

        Objects.requireNonNull(status);

        return Stream.of(values())
                .filter(bean -> bean.status.equals(status))
                .findAny()
                .orElseThrow(
                        () -> new IllegalArgumentException(status + " not exists")
                );
    }
}
